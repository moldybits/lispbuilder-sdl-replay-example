(in-package :replay)

(defparameter *state* nil)

(defun sdl-key->snes (key)
  (getf '(:sdl-key-z :b
          :sdl-key-x :a
          :sdl-key-a :y
          :sdl-key-s :x)
        key))

(defun draw-button (x y color pressed)
  (let ((color (sdl:color :r (first color)
                          :g (second color)
                          :b (third color))))
    (if pressed
        (sdl:draw-filled-circle-* x y 32
                                  :color color
                                  :stroke-color (sdl:color :r 255 :g 255 :b 255 :a 255))
        (sdl:draw-filled-circle-* x y 32
                                  :color (sdl:color :r 0 :g 0 :b 0)
                                  :stroke-color (sdl:color :r 255 :g 255 :b 255 :a 255)))))

(defun handle-button-event (state event)
  (case (first event)
    (:press
     (push (second event) (pressed state)))
    (:release
     (setf (pressed state) (remove (second event) (pressed state))))))

;;;; state ;;;;
(defclass state ()
  ((frame :accessor frame :initform 0)
   (pressed :accessor pressed :initform nil)
   (input-history :accessor input-history :initarg :input-history :initform nil)))

(defmethod render ((state state))
  (draw-button 128 196 '(255 255   0) (find :b (pressed state)))
  (draw-button 196 128 '(255   0   0) (find :a (pressed state)))
  (draw-button 64  128 '(  0 255   0) (find :y (pressed state)))
  (draw-button 128  64 '(  0   0 255) (find :x (pressed state))))

(defmethod tick ((state state))
  (incf (frame state)))

;;;; state-play ;;;;
(defclass state-play (state) ())

(defmethod handle-event ((state state-play) event)
  (push (cons (frame state) event) (input-history state))
  (handle-button-event state event))

;;;; state-replay ;;;;
(defclass state-replay (state) ())

(defmethod handle-event ((state state-replay) event))

(defmethod tick :after ((state state-replay))
  (let ((event (cdr (assoc (frame state) (input-history state)))))
    (when event
      (handle-button-event state event))))

;;;; start ;;;;
(defun start ()
  (sdl:with-init ()
    (sdl:window 256 256)
    (setf (sdl:frame-rate) 60)

    (sdl:with-events ()
      (:quit-event () t)
      (:key-down-event (:key key)
        (when (eq :sdl-key-escape key)
          (sdl:push-quit-event))
        (let ((k (sdl-key->snes key)))
          (when k
            (handle-event *state* (list :press k)))))
      (:key-up-event (:key key)
        (let ((k (sdl-key->snes key)))
          (when k
            (handle-event *state* (list :release k)))))
      (:idle ()
        (tick *state*)
        (sdl:clear-display sdl:*black*)
        (render *state*)
        (sdl:update-display)))))

(defun play ()
  (setf *state* (make-instance 'state-play))
  (start))

(defun replay (&optional (input-history (input-history *state*)))
  (setf *state* (make-instance 'state-replay :input-history input-history))
  (start))

(defun help (&optional prefix)
  (let ((prefix (or prefix ; how to do this properly?
                    (if (string= "REPLAY" (package-name *package*)) "" "replay:"))))
    (format t "~&--- replay.lisp ---~%")
    (format t "(~Aplay) to start.~%" prefix)
    (terpri)
    (format t "Use `z' `x' `s' `a' to light up buttons.~%")
    (format t "ESC to quit.~%")
    (terpri)
    (format t "(~Areplay) to see it again!" prefix)))

(help "replay:")
