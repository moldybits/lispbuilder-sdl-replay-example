(asdf:defsystem :replay
  :description "Replaying input in lispbuilder-sdl."
  :author "moldybits"
  :version "0.0.1"
  :depends-on (lispbuilder-sdl)
  :serial t
  :pathname ""
  :components
  ((:file "packages")
   (:file "replay")))
